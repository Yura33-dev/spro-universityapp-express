/* eslint-disable no-console */
import path from 'path';
import express, { Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import exceptionsFilter from './midlewares/exception.filter';
import AppDataSource from '../configs/database/data-source';

dotenv.config();
const app = express();
const staticFilesPath = path.join(__dirname, '../', 'public');

// --midlewares
app.use(cors());
app.use(bodyParser.json());

AppDataSource.initialize()
  .then(() => console.log('Typeorm connected to database'))
  .catch((error) => console.log('Error:', error));
// ---
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/public', express.static(staticFilesPath));
app.use('*', (req: Request, res: Response) => {
  res.status(404).json({ status: 404, message: 'Route not found' });
});
app.use(exceptionsFilter); // should be a last one

export default app;

// process.env.USERNAME = 'test';
// process.env.PASSWORD = 'test';
// process.env.DATABASE_PORT = '5432';
// process.env.DATABASE_HOST = '0.0.0.0';
// process.env.DATABASE_USER = 'test';
// process.env.DATABASE_PASSWORD = 'test';
// process.env.DATABASE_NAME = 'test';
