import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/http-exception';
import HttpStatuses from '../enums/httpStatusCodes';

const exceptionsFilter = (
  error: HttpException,
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction,
) => {
  const status = error.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Something went wrong';
  res.status(status).send({ status, message });
};

export default exceptionsFilter;
