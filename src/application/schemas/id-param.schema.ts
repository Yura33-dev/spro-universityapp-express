import Joi from 'joi';

const idSchema = Joi.object<{ id: string }>({
  id: Joi.string().hex().length(24).required(),
});

export default idSchema;
