import { DataSource } from 'typeorm';
import databaseConfiguration from './database-config';

const AppDataSource = new DataSource(databaseConfiguration());

export default AppDataSource;
