import { Column, Entity, OneToMany } from 'typeorm';
import CoreEntity from '../../application/entities/core.entity';
// eslint-disable-next-line import/no-cycle
import Student from '../../students/entities/student.entity';

@Entity({ name: 'groups' })
class Group extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group)
  students: Student[];
}

export default Group;
