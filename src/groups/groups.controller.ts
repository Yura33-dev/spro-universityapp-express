import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as groupsService from './groups.service';
import { IGroupCreateRequest } from './types/group-create-interface';
import { IGroupUpdateRequest } from './types/group-update-interface';
import HttpStatuses from '../application/enums/httpStatusCodes';

export const getGroups = async (req: Request, res: Response) => {
  const groups = await groupsService.getGroups();
  res.json(groups);
};

export const getGroupById = async (req: Request, res: Response) => {
  const { id } = req.params;
  const group = await groupsService.getGroupById(id);
  res.json(group);
};

export const createGroup = async (
  req: ValidatedRequest<IGroupCreateRequest>,
  res: Response,
) => {
  const newGroup = await groupsService.createGroup(req.body);
  res.status(HttpStatuses.CREATED).json(newGroup);
};

export const updateGroupById = async (
  req: ValidatedRequest<IGroupUpdateRequest>,
  res: Response,
) => {
  const { id } = req.params;
  const editedGroup = await groupsService.updateGroupById(id, req.body);
  res.json(editedGroup);
};

export const deleteGroupById = async (req: Request, res: Response) => {
  const { id } = req.params;
  const deleteGroup = await groupsService.deleteGroupById(id);
  res.json(deleteGroup);
};
