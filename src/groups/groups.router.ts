import { Router } from 'express';
import validator from '../application/midlewares/validation.middleware';
import idSchema from '../application/schemas/id-param.schema';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import {
  groupCreateSchema,
  groupUpdateSchema,
} from './groups.schema-validation';

const router = Router();

router.get('/', controllerWrapper(groupsController.getGroups));

router.get(
  '/:id',
  // validator.params(idSchema),
  controllerWrapper(groupsController.getGroupById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

router.patch(
  '/:id',
  // validator.params(idSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  // validator.params(idSchema),
  controllerWrapper(groupsController.deleteGroupById),
);

export default router;
