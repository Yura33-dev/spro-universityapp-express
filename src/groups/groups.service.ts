import HttpStatuses from '../application/enums/httpStatusCodes';
import HttpException from '../application/exceptions/http-exception';
import AppDataSource from '../configs/database/data-source';
import Student from '../students/entities/student.entity';
import Group from './entities/group.entity';
import { IResponseForUpdateGroup } from './types/group-responses-interfaces';

const groupsRepository = AppDataSource.getRepository(Group);

export const createGroup = async (
  newGroupSchema: Omit<Group, 'id'>,
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: newGroupSchema.name,
    },
  });

  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with the same name already exist!',
    );
  }

  return groupsRepository.save(newGroupSchema);
};

export const getGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoin('groups.students', 'students')
    .addSelect([
      'students.id',
      'students.name',
      'students.surname',
      'students.age',
      'students.email',
    ])
    .getMany();
  return groups;
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoin('groups.students', 'students')
    .addSelect([
      'students.id',
      'students.name',
      'students.surname',
      'students.age',
      'students.email',
    ])
    .where('groups.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }
  return group;
};

export const updateGroupById = async (
  groupId: string,
  groupNewData: Partial<Group>,
): Promise<IResponseForUpdateGroup> => {
  const result = await groupsRepository.update(groupId, groupNewData);

  if (!result.affected)
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  return {
    ok: true,
    message: 'Group data updated successfully',
    data: groupNewData,
  };
};

export const deleteGroupById = async (
  id: string,
): Promise<IResponseForUpdateGroup> => {
  const findGroup = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoin('groups.students', 'students')
    .addSelect('students.id')
    .where('groups.id = :id', { id })
    .getOne();

  if (!findGroup)
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');

  const findStudentsIdInGroup = findGroup.students.map((student) => student.id);

  for (let i = 0; i < findStudentsIdInGroup.length; i += 1) {
    groupsRepository
      .createQueryBuilder()
      .update(Student)
      .set({ groupId: null })
      .where('id = :id', { id: findStudentsIdInGroup[i] })
      .execute();
  }

  await groupsRepository.delete(id);

  return {
    ok: true,
    message: 'Group deleted successfully',
    data: {
      groupId: id,
    },
  };
};
