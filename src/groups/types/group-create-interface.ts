import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import Group from '../entities/group.entity';

export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Group, 'id'>;
}
