export interface IResponseForUpdateGroup {
  ok: boolean;
  message: string;
  data: object;
}
