import dotenv from 'dotenv';
import app from './application/app';

dotenv.config();
const port = process.env.SERVER_PORT;
const startServer = async () => {
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`Server started at port: ${port}...`);
  });
};
startServer();
