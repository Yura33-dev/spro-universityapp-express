import { Column, Entity } from 'typeorm';
import CoreEntity from '../../application/entities/core.entity';

@Entity({ name: 'lectors' })
class Lectors extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;
}

export default Lectors;
