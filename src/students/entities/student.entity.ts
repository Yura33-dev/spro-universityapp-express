import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import CoreEntity from '../../application/entities/core.entity';
// eslint-disable-next-line import/no-cycle
import Group from '../../groups/entities/group.entity';

@Entity({ name: 'students' })
class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    name: 'image_path',
    type: 'varchar',
    nullable: true,
    default: null,
  })
  imagePath: string | null;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number | null;
}

export default Student;
