import groups from '../../groups/groups-data';
import { IStudent } from '../types/student.interface';

const isGroupExist = (newGroupId: Pick<IStudent, 'groupId'>): boolean =>
  !!groups.find(({ id }) => id === newGroupId.groupId);

export default isGroupExist;
