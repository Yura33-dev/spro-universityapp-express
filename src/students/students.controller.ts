import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as studentsService from './students.service';

import {
  IStudentAddGroupRequest,
  IStudentUpdateRequest,
} from './types/student-update-interface';
import { IStudentCreateRequest } from './types/student-create-interface';
import HttpStatuses from '../application/enums/httpStatusCodes';

export const getAllStudents = async (req: Request, res: Response) => {
  const students = await studentsService.getAllStudents();
  res.json(students);
};

// export const getAllStudentsWithGroupname = (req: Request, res: Response) => {
//   const students = studentsService.getAllStudentsWithGroupname();
//   res.json(students);
// };

export const getStudentById = async (
  req: Request<{ id: string }>,
  res: Response,
) => {
  const { id } = req.params;
  const student = await studentsService.getStudentById(id);
  res.json(student);
};

export const createStudent = async (
  req: ValidatedRequest<IStudentCreateRequest>,
  res: Response,
) => {
  const student = await studentsService.createStudent(req.body);
  res.status(HttpStatuses.CREATED).json(student);
};

export const updateStudentById = async (
  req: ValidatedRequest<IStudentUpdateRequest>,
  res: Response,
) => {
  const { id } = req.params;
  const student = await studentsService.updateStudentById(id, req.body);
  res.json(student);
};

// export const addImage = async (
//   req: Request<{ id: string; file: Express.Multer.File }>,
//   res: Response,
// ) => {
//   const { id } = req.params;
//   const { path } = req.file ?? {};
//   const student = await studentsService.addImage(id, path);
//   res.status(HttpStatuses.CREATED).json(student);
// };

export const deleteStudentByid = async (
  req: Request<{ id: string }>,
  res: Response,
) => {
  const { id } = req.params;
  const student = await studentsService.deleteStudentByid(id);
  res.status(HttpStatuses.NO_CONTENT).json(student);
};

export const addGroupByStudentId = async (
  req: ValidatedRequest<IStudentAddGroupRequest>,
  res: Response,
) => {
  const { id } = req.params;
  const student = await studentsService.setGroupByStudentId(id, req.body);
  res.json(student);
};

// export const getStudentImageById = (
//   req: Request<{ id: string }>,
//   res: Response,
// ) => {
//   const { id } = req.params;
//   const student = studentsService.getStudentImageById(id);
//   res.json(student);
// };
