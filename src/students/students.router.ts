import { Router } from 'express';
import validator from '../application/midlewares/validation.middleware';
import { studentCreateSchema, studentUpdateSchema } from './student.schema';
import idSchema from '../application/schemas/id-param.schema';
import * as studentsController from './students.controller';
import uploadMiddleware from '../application/midlewares/upload.middleware';
import controllerWrapper from '../application/utils/controller-wrapper';

const router = Router();

router.get('/', studentsController.getAllStudents);

router.get(
  '/:id',
  // validator.params(idSchema),
  controllerWrapper(studentsController.getStudentById),
);

// router.get(
//   '/:id/image',
//   validator.params(idSchema),
//   controllerWrapper(studentsController.getStudentImageById),
// );

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);
router.patch(
  '/:id',
  // validator.params(idSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);
// router.patch(
//   '/:id/image',
//   uploadMiddleware.single('file'),
//   controllerWrapper(studentsController.addImage),
// );
router.patch(
  '/:id/groups',
  controllerWrapper(studentsController.addGroupByStudentId),
);
router.delete(
  '/:id',
  // validator.params(idSchema),
  controllerWrapper(studentsController.deleteStudentByid),
);

export default router;
