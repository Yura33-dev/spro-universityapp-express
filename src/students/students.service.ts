import HttpStatuses from '../application/enums/httpStatusCodes';
import HttpException from '../application/exceptions/http-exception';
import AppDataSource from '../configs/database/data-source';
import Student from './entities/student.entity';
import { IResponseForUpdateStudent } from './types/student-responses-interfaces';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (): Promise<Student[]> => {
  const students = await studentsRepository
    .createQueryBuilder('students')
    .select([
      'students.id as id',
      'students.createdAt as createdAt',
      'students.updatedAt as updatedAt',
      'students.name as name',
      'students.surname as surname',
      'students.email as email',
      'students.age as age',
      'students.imagePath as imagePath',
    ])
    .leftJoin('students.group', 'group')
    .addSelect('group.name as "groupName"')
    .getRawMany();
  return students;
};

export const getStudentById = async (id: string): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
      'student.groupId as groupId',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student)
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  return student;
};

export const createStudent = async (
  newStudent: Omit<Student, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: newStudent.email,
    },
  });
  if (student)
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'Student already exist');

  return studentsRepository.save(newStudent);
};

export const deleteStudentByid = async (
  id: string,
): Promise<IResponseForUpdateStudent> => {
  const result = await studentsRepository.delete(id);
  if (!result.affected)
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  return {
    ok: true,
    message: 'Student deleted successfully',
    data: {
      studentsId: id,
    },
  };
};

export const updateStudentById = async (
  id: string,
  updateData: Partial<Student>,
): Promise<IResponseForUpdateStudent> => {
  const result = await studentsRepository.update(id, updateData);
  if (!result.affected)
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not not found');
  return {
    ok: true,
    message: 'Data updated successfully',
    data: updateData,
  };
};

export const setGroupByStudentId = async (
  studentId: string,
  groupId: Pick<Student, 'groupId'>,
): Promise<IResponseForUpdateStudent> => {
  const result = await studentsRepository.update(studentId, groupId);
  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return {
    ok: true,
    message: 'Group updated successfully',
    data: groupId,
  };
};

// export const addImage = async (id: string, filePath?: string) => {
//   if (!filePath)
//     throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');

//   const findStundet = studentsModel.getStudentById(id);
//   if (!findStundet)
//     throw new HttpException(HttpStatuses.BAD_REQUEST, 'Student not found');

//   try {
//     const imageId = ObjectID().toHexString();
//     const imageExt = path.extname(filePath);
//     const imageName = imageId + imageExt;

//     const studentsDirectoryName = 'students';
//     const studentsDirectoryPath = path.join(
//       __dirname,
//       '../',
//       'public',
//       studentsDirectoryName,
//     );
//     const newImagePath = path.join(studentsDirectoryPath, imageName);
//     const imagePath = `${studentsDirectoryName}/${imageName}`;

//     await fs.rename(filePath, newImagePath);

//     const updatedStudent = updateStudentById(id, { imagePath });

//     return updatedStudent;
//   } catch (e) {
//     await fs.unlink(filePath);
//   }
// };

// export const deleteStudentByid = (id: string) => {
//   const student = studentsModel.deleteStudentByid(id);
//   if (!student)
//     throw new HttpException(HttpStatuses.BAD_REQUEST, 'Student not found');
// };

// export const getStudentImageById = (
//   studentId: string,
// ): Pick<IStudent, 'imagePath'> => {
//   const student = studentsModel.getStudentById(studentId);
//   if (!student)
//     throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');

//   if ('imagePath' in student) {
//     return { imagePath: student.imagePath };
//   }
//   throw new HttpException(HttpStatuses.OK, 'Student have not an image');
// };
