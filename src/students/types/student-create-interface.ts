import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import Student from '../entities/student.entity';

export interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Student, 'id'>;
}
