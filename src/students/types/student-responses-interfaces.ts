import Student from '../entities/student.entity';

export type addStudentGroupErrorResponse = Pick<Student, 'groupId'> & {
  messageError: string;
};

export interface IResponseForUpdateStudent {
  ok: boolean;
  message: string;
  data: object;
}
