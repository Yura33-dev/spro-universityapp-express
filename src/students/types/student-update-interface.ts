import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import Student from '../entities/student.entity';

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Student>;
}

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<Student, 'groupId'>;
}
